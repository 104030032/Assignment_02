var resultState = { 
    create: function() {
    // Add a background image 
    var bg = game.add.image(0, 0, 'background');
    bg.width=400;
    bg.height=500; 
    // Display the name of the game
    var nameLabel = game.add.text(game.width/2, 80, 'Game Over', 
    { font: '50px Arial', fill: '#ffffff' }); 
    nameLabel.anchor.setTo(0.5, 0.5);
    // Show the score at the center of the screen
    var scoreLabel = game.add.text(game.width/2,  game.height/2, 
    'score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' }); 
    scoreLabel.anchor.setTo(0.5, 0.5);
    // Explain how to start the game
    var startLabel = game.add.text(game.width/2, game.height-80, 
    'press the ENTER key to replay', { font: '25px Arial', fill: '#ffffff' }); 
    startLabel.anchor.setTo(0.5, 0.5);
    // Create a new Phaser keyboard variable: the up arrow key 
    // When pressed, call the 'start'
    var exit = game.input.keyboard.addKey(Phaser.Keyboard.ESC); 
    exit.onDown.add(this.escape, this);     
    
    game.time.events.add(Phaser.Timer.SECOND * 0.1, this.escape, this);

    var replay = game.input.keyboard.addKey(Phaser.Keyboard.ENTER); 
    replay.onDown.add(this.restart, this); 
    },
    
    restart: function() { 
        game.state.start('play'); 
    },

    escape: function() {
        $("form").removeClass("hide");
        $("#rules").addClass("hide");
        document.getElementById('score').innerHTML=game.global.score;
    }       
}; 

var player;
var platforms = [];
var leftWall;
var rightWall;
var ceiling;
var text;
var hitSound;
var deadSound;
var ceiling_height=50;
//var SCORE = document.getElementById("score");
var LIFE = document.getElementById("life");
var RECOVER = document.getElementById("recover");
//////////////////////////////////////////////////////////////////////////////////////////
var playState = {    
    preload: function() {        
    },
//////////////////////////////////////////////////////////////////////////////////////////
    create: function() {
        game.global.score = 0;
        game.global.life = 3;
        game.global.recover= 10;
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.physics.arcade.checkCollision.down = false;
        //for sound
        this.jumpSound = game.add.audio('jump');
        hitSound = game.add.audio('hit');
        deadSound = game.add.audio('dead');
        this.bg = game.add.tileSprite(0, ceiling_height,400,450, 'bg');
        /*time events */
        game.time.events.add(Phaser.Timer.SECOND * 0.1, this.hide, this);
        
        createBound();
        createPlayer();
        createText();
        
        player.events.onOutOfBounds.add(function(){
             alert('Game over!');
            location.reload();
        }, this);
        this.cursor = game.input.keyboard.createCursorKeys();
    },

    /// Call moving function in each frame.
    update: function() {
        game.physics.arcade.collide(player, platforms,effect);
        game.physics.arcade.collide(player, leftWall);
        game.physics.arcade.collide(player, rightWall);
        //game.physics.arcade.collide(player, ceiling);
        updateFloor();
        createFloor();
        checkGameOver();
        this.movePlayer();
        checkAddlife();
        text.setText("life:" + game.global.life);
        text2.setText("get life in: " + game.global.recover +"  jump");
    },
 ////////////////////////////////////////////////////////////////////////////////////////// 
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            player.body.velocity.x = -200;
            player.facingLeft = true;
            player.animations.play('leftwalk');
        }
        else if (this.cursor.right.isDown) { 
            player.body.velocity.x = 200;
            player.facingLeft = false; 
            player.animations.play('rightwalk');
        }     
        // If neither the right or left arrow key is pressed
        else {
            player.body.velocity.x = 0;
            player.frame = 8;
            player.animations.stop();
        }    
    },

    hide: function() {
        $("form").addClass("hide");
        $("#rules").removeClass("hide");
    },
};

function createPlayer () {
    player = game.add.sprite(200, 100, 'player');
    //player.anchor.setTo(0.5, 0.5);
    game.physics.arcade.enable(player);
    player.body.gravity.y = 500;
    player.facingLeft = false;
    player.animations.add('leftwalk', [0, 1, 2, 3], 8, true);
    player.animations.add('rightwalk', [9, 10, 11, 12], 8, true);
}

function createText () {
    var style = {fill: '#ffffff', fontSize: '20px'}
    text = game.add.text(10, 10, '', style);
    text2 = game.add.text(200, 10, '', style)
}

function createBound()
{
    leftWall = game.add.sprite(0, ceiling_height, 'wall');
    leftWall.height = 500;
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;

    rightWall = game.add.sprite(383, ceiling_height, 'wall');
    rightWall.height = 500;
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;

    ceiling = game.add.sprite(0, ceiling_height, 'ceiling');
}

var lastTime = 0;

function createFloor()
{
    if(game.time.now > lastTime + 700) {
        lastTime = game.time.now;
        createOneFloor();
    }
}

function createOneFloor()
{
    var x = Math.random() * (400 - 96 - 40) + 20;
    var types = Math.random()*100; 

    if (types<50){
        platform = game.add.sprite(x,500, 'normal');
    }else if (types<60){
        platform = game.add.sprite(x, 500, 'conveyorLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    }else if (types<70){
        platform = game.add.sprite(x, 500, 'conveyorRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    }else if (types<80){
        platform = game.add.sprite(x, 500, 'nails');
        platform.anchor.setTo(0,1);
    }else if (types<90){
        platform = game.add.sprite(x, 500, 'fake');
        platform.animations.add('turn', [1, 2, 3, 4, 5, 0]);
    }else{
        platform = game.add.sprite(x, 500, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.frame = 3;
    }    
    
    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);

    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}

function updateFloor () {
    for(var i=0; i<platforms.length; i++) {
        platforms[i].body.y -= 2.5;

        if(platforms[i].body.y < ceiling_height) {
            platforms[i].destroy();
            platforms.splice(i, 1);
        }
    }
}

function checkGameOver () {
    if(game.global.life==0 || player.body.y > 550 || player.body.y < 58) {
            gameOver();
            text.setText("life: 0");
    }
}

function gameOver () {
    platforms.forEach(function(i) {i.destroy()});
    platforms = [];
    deadSound.play();
    game.state.start('result');
}

function effect (player, platform) {
    
    if(platform.key == 'conveyorLeft') {
        if(player.touchOn !== platform) {
            player.touchOn = platform;
            game.global.score += 5;
            game.global.recover -= 1;
            hitSound.play();
        }
        player.body.x -= 2;
    }
    if(platform.key == 'conveyorRight') {
        if(player.touchOn !== platform) {
            player.touchOn = platform;
            game.global.score += 5;
            game.global.recover -= 1;
            hitSound.play();
        }
        player.body.x += 2;
    }
    if(platform.key == 'trampoline') {
        if(player.touchOn !== platform) {
            player.touchOn = platform;
            game.global.score += 5;
            game.global.recover -= 1;
            hitSound.play();
        }
        player.body.velocity.y = -300;
        platform.play('jump');
    }
    if(platform.key == 'nails') {
        if(player.touchOn !== platform) {
            player.touchOn = platform;
             game.global.life -= 1 ;
             game.global.recover = 10;
             hitSound.play();
        }       
    }
    if(platform.key == 'normal') {
        if(player.touchOn !== platform) {
            player.touchOn = platform;
            game.global.score += 5;
            game.global.recover -= 1;
            hitSound.play();
        }
        
    }
    if(platform.key == 'fake') {
        if(player.touchOn !== platform) {
            setTimeout(function() {
                platform.play('turn');
                platform.body.checkCollision.up = false;
            }, 150);
            player.touchOn = platform;
            game.global.score += 5;
            game.global.recover -= 1;
        }
    }
}

function checkAddlife()
{   
    
    if(game.global.life==3)
    {
        text2.visible = false;
    }
    else{
        text2.visible = true;
    }

    if(text2.visible){
        if(game.global.recover==0 && game.global.life < 3)
        {
        game.global.life +=1;
        game.global.recover = 10;
        }
    }
}

function init()
{
    var score = firebase.database().ref('score_list').orderByChild('SCORE');
    var inform=[];
    var first_count = 0;
    var second_count = 0;
    var back=document.getElementById("go_back");

  score.once("value")
  .then(function(snapshot){
    snapshot.forEach(function(childSnapshot) {
      var childData = childSnapshot.val();
      inform[inform.length]='<tr><td>'+childData.ID+'</td><td>'+childData.SCORE+'</td>'+'</tr>';
      first_count += 1;
    });
    inform.reverse();    
    document.getElementById('message').innerHTML = inform.join('');

    score.on('child_added', function (data) {
        second_count += 1;
        if (second_count > first_count) {
            var childData = data.val();
            inform[inform.length]='<tr><td>'+childData.ID+'</td><td>'+childData.SCORE+'</td>'+'</tr>';
            inform.reverse();  
            document.getElementById('message').innerHTML = inform.join('');
        }
    });
  });

  back.addEventListener('click', function () {
      window.close();             
  });


}

window.onload = function () {
    init();

}


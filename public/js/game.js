var canvas=document.getElementById("canvas");
var game = new Phaser.Game(400, 500, Phaser.AUTO, "canvas");
game.global = { score: 0,life:3, recover:10};

game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('result',resultState);
game.state.start('boot');


# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Reference
    https://github.com/wacamoto/NS-Shaft-Tutorial

## Checking
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  Yes |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  Yes  |
|         All things in your game should have correct physical properties and behaviors.         |  Yes  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  Yes  |
| Add some additional sound effects and UI to enrich your game.                                  |  Yes  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  Yes  |
| Other creative features in your game (describe on README.md)                                   |  Yes  | 

## Other function
1. 生命系統: 每次遊戲有三條性命，用完才會死(但碰到天花板是直接死)
2. 回血系統: 只要連續10次跳躍都沒碰到有針的平台，就可以回一條命(兩者都會直接顯示在螢幕上讓你知道)
3. 介面: 分成左半和右半，右半是遊戲，左半部分則會根據不同的state顯示不同的東西(規則or姓名輸入欄)

## Website
https://104030032.gitlab.io/Assignment_02
https://go-down-stairs-15406.firebaseapp.com/

